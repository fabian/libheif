libheif (1.16.2-1) unstable; urgency=medium

  [ Sebastian Ramacher ]
  * Use libgdk-pixbuf-2.0-dev
  * Set MA for heif-gdk-pixbuf and heif-thumbnailer (Closes: #981551)

  [ Joachim Bauch ]
  * New upstream version 1.16.2
  * Update symbols for new upstream version.
  * Build codecs as plugins and separate packages that are pulled using
    "Recommends".
  * Add codec plugins using "rav1e" and "svtenc".
  * Remove duplicate MA entry in heif-gdk-pixbuf.
  * d/rules: No need to override "dh_autoreconf", no longer used.

 -- Joachim Bauch <bauch@struktur.de>  Tue, 20 Jun 2023 11:37:08 +0200

libheif (1.15.1-1) unstable; urgency=medium

  * Team upload
  * New upstream version 1.15.1
    - Fix CVE-2023-0996 (Closes: #1032101)
    - Do not fail if no plugin directory is available (Closes: #1029668)
  * debian/libheif1.symbols: Add new symbols
  * debian/*.install: Update for new upstream release

 -- Sebastian Ramacher <sramacher@debian.org>  Thu, 02 Mar 2023 00:09:34 +0100

libheif (1.14.2-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + heif-gdk-pixbuf: Add Multi-Arch: same.

  [ Joachim Bauch ]
  * New upstream version 1.14.2
  * Update symbols for new upstream version.
  * Switch to cmake and use arch-dependent include directory.
  * Update "Standards-Version" to 4.6.2

 -- Joachim Bauch <bauch@struktur.de>  Tue, 17 Jan 2023 10:17:32 +0100

libheif (1.13.0-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Joachim Bauch ]
  * New upstream version 1.13.0
  * Update symbols for new upstream version.
  * Update "Standards-Version" to 4.6.1

 -- Joachim Bauch <bauch@struktur.de>  Fri, 02 Sep 2022 13:20:39 +0200

libheif (1.12.0-2) unstable; urgency=medium

  * Team upload
  * Upload to unstable

 -- Sebastian Ramacher <sramacher@debian.org>  Tue, 31 Aug 2021 22:37:31 +0200

libheif (1.12.0-1) experimental; urgency=medium

  * New upstream version 1.12.0
  * Update symbols for new upstream version.
  * Bump watch file to version 4.

 -- Joachim Bauch <bauch@struktur.de>  Mon, 10 May 2021 15:51:05 +0200

libheif (1.11.0-1) unstable; urgency=medium

  * Imported Upstream version 1.11.0
  * Remove patch applied upstream.
  * Update symbols for new upstream version.

 -- Joachim Bauch <bauch@struktur.de>  Tue, 02 Feb 2021 10:28:05 +0100

libheif (1.10.0-2) unstable; urgency=medium

  * Team upload
  * debian/control: Add missing Depends to libheif-dev (Closes: #978176)
  * debian/patches: Initialize valid_values to nullptr

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 02 Jan 2021 15:23:12 +0100

libheif (1.10.0-1) unstable; urgency=medium

  * Imported Upstream version 1.10.0
  * Update "Standards-Version" to 4.5.1
  * Remove patch applied upstream.
  * Update symbols for new upstream version.

 -- Joachim Bauch <bauch@struktur.de>  Wed, 16 Dec 2020 17:23:17 +0100

libheif (1.9.1-1) unstable; urgency=medium

  * Imported Upstream version 1.9.1
  * Update symbols for new upstream version.
  * Build with libde265 >= 1.0.7 to get NCLX API.
  * Update to debhelper compat level 13.
  * Add patch to support building against system-installed dav1d.
  * Build with "libdav1d-dev" to get faster AVIF decoder.

 -- Joachim Bauch <bauch@struktur.de>  Mon, 28 Sep 2020 09:27:40 +0200

libheif (1.8.0-1) unstable; urgency=medium

  * Add "Rules-Requires-Root", no root necessary.
  * Imported Upstream version 1.8.0
  * Update symbols for new upstream version.
  * Build with "libaom-dev" to get AVIF support.
  * Update "Standards-Version" to 4.5.0

 -- Joachim Bauch <bauch@struktur.de>  Thu, 27 Aug 2020 16:07:58 +0200

libheif (1.6.1-1) unstable; urgency=medium

  * Imported Upstream version 1.6.1

 -- Joachim Bauch <bauch@struktur.de>  Fri, 20 Dec 2019 10:31:19 +0100

libheif (1.6.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1

  [ Joachim Bauch ]
  * Imported Upstream version 1.6.0
  * Update symbls for new upstream version.
  * Install man pages.

 -- Joachim Bauch <bauch@struktur.de>  Fri, 08 Nov 2019 14:23:21 +0100

libheif (1.5.1-1) unstable; urgency=medium

  * Imported Upstream version 1.5.1
  * Update to debhelper compat level 12 and add debian/not-installed
  * Enable hardening.
  * Stop parsing changelog manually.
  * Fix "get-head-source" and don't include date in filename.
  * Specify "Build-Depends-Package" in symbols.

 -- Joachim Bauch <bauch@struktur.de>  Fri, 30 Aug 2019 10:30:36 +0200

libheif (1.5.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat
  * Bump Standards-Version to 4.4.0

  [ Joachim Bauch ]
  * Imported Upstream version 1.5.0
  * Update symbols for new upstream version.
  * Add copyright entries for new files in test/
  * Add missing copyright entry for files in scripts/
  * The examples are MIT licensed since 1.4.0
  * Remove patches no longer needed (fixed upstream).

  [ Mattia Rizzolo ]
  * Drop now unneeded debian/source/include-binaries

 -- Joachim Bauch <bauch@struktur.de>  Fri, 16 Aug 2019 16:29:09 +0200

libheif (1.4.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * debian/patches: Apply upstream fixes for CVE-2019-11471. (Closes: #928210)

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 06 Jul 2019 15:37:07 +0200

libheif (1.4.0-1) experimental; urgency=medium

  * Imported Upstream version 1.4.0
  * Add new package containing the gdk-pixbuf loader.
  * Update "Standards-Version" to 4.3.0
  * Update symbols for new upstream version.

 -- Joachim Bauch <bauch@struktur.de>  Tue, 02 Apr 2019 10:17:10 +0200

libheif (1.3.2-1) unstable; urgency=medium

  * Imported Upstream version 1.3.2
  * Update "Standards-Version" to 4.1.4
  * Update symbols for new upstream version.

 -- Joachim Bauch <bauch@struktur.de>  Thu, 21 Jun 2018 15:40:05 +0200

libheif (1.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field

  [ Joachim Bauch ]
  * Imported Upstream version 1.2.0
  * Remove patch now in upstream.
  * Update symbols for new upstream version.
  * Add new package "heif-thumbnailer".

 -- Joachim Bauch <bauch@struktur.de>  Mon, 28 May 2018 16:12:56 +0200

libheif (1.1.0-2) unstable; urgency=medium

  * Add patch to fix compile errors on mips, ppc and other platforms.

 -- Joachim Bauch <bauch@struktur.de>  Thu, 19 Apr 2018 09:04:15 +0200

libheif (1.1.0-1) unstable; urgency=medium

  [ Joachim Bauch ]
  * d/control: Set Maintainer to Debian Multimedia Maintainers
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Joachim Bauch ]
  * Use default branch/tag names.
  * Imported Upstream version 1.1.0
  * Add dependency on "libx265-dev".
  * Update symbols for new upstream version.

 -- Joachim Bauch <bauch@struktur.de>  Wed, 18 Apr 2018 16:28:10 +0200

libheif (1.0.0-1) unstable; urgency=medium

  * Initial release. (Closes: #888278)

 -- Joachim Bauch <bauch@struktur.de>  Sun, 18 Mar 2018 14:55:50 +0100
